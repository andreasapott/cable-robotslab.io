---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
translation: 0
rotation: 0
description: insert a short description of the motion pattern here
---

**Insert Lead paragraph here.**

## Start With Heading Level 2


## Documentation


### `description`

```yaml
description: A short description of the motion pattern
```


### `translation`

Number of linear degrees of freedom of the motion pattern

#### Sample

```yaml
translation: 3
```


### `rotation`

Number of rotational degrees of freedom of the motion pattern

#### Sample
```yaml
rotation: 3
```

