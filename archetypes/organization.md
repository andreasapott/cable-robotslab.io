---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
short_title: ""
institution: ""
short_institution: ""
location: City, Country
website: https://example.com 
---

**Insert Lead paragraph here.**

## Start With Heading Level 2


## Documentation

### `short_title`

Short form of the organization's title.

#### Sample
```yaml
short_title: PME
```

### `institution`

Governing institution of the organization like the embedding university of research facility.

#### Sample

```yaml
institution: Delft University of Technolog
```

### `short_institution`

Short form of the governing institution's name.

#### Sample

```yaml
short_institution: TU Delft
```

### `location`

Where the organization is located.
Is a markdown enabled field.

#### Sample
```yaml
location: Delft, the Netherlands
```

### `website`

Link to the organization's website, if any.

#### Sample
```yaml
website: http://pme.tudelft.nl
```
