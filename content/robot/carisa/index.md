---
title: "CaRISA"
date: 2020-04-27T18:14:20+02:00
long_title: Cable Robot for Inspection and Scanning of Art objects
description: "Mounted with various imaging devices like UV/IR cameras and spectrometer, this robot aims to scan paintings with high accuracy."
motionpattern:
    - 2r3t
    - 3r3t
organization:
    - tudelft-pme
---
Mounted with various imaging devices like UV/IR cameras and spectrometer, this robot aims to scan paintings with high accuracy.