---
title: Fraunhofer Institute for Manufacturing Engineering and Automation IPA
date: 2020-05-07T17:49:57+02:00
short_title: IPA
institution: Fraunhofer-Gesellschaft
short_institution: FhG
location: Stuttgart, Germany
website: https://www.ipa.fraunhofer.de
resources: 
    - name: cover
      src: cover.jpg
    - name: avatar
      src: avatar.png
---

Fraunhofer IPA – one of the Fraunhofer-Gesellschaft’s largest institutes – was founded in 1959 and employs almost 1000 workers. Our future and lead topics include the biointelligent value chain, digital transformation in the context of Industrie 4.0, energy storage systems, frugal manufacturing systems, artificial intelligence in automation, lightweight engineering and resource efficiency.

The focus of research and development work at Fraunhofer IPA is on organizational and technological issues related to the manufacturing industry. We develop, test and implement methods, components and devices right up to complete machines and production lines. Fraunhofer IPA’s 15 specialist departments cover the entire field of manufacturing engineering. They are coordinated by six business units and work on an interdisciplinary basis with industrial enterprises from the following sectors: automotive, machinery & equipment, electronics & microsystems, energy, medical engineering & biotechnology and the process industry.
