---
title: Cable Robots
linkTitle: Home
sections:
    - robot
    - organization
    - publication
    - researcher
---

<div class="row">
<div class="col-md-8 offset-md-2">
{{< cards/card class="bg-light card-inactive" >}}
{{< cards/body class="text-center text-muted" >}}
Find out more on cable robots on these websites and connect with the community.

You can browse through our list of [cable robots](robot), which [researchers](researcher) are behind these robots, and which [organizations](organization) all around the world are researching in this field.
{{< /cards/body >}}
{{< /cards/card >}}
</div>
</div>
