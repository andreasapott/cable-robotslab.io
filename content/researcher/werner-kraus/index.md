---
title: "Werner Kraus"
date: 2020-05-07T16:46:50+02:00
toc: true
organization:
    - fhg-ipa
location: Stuttgart, Germany
social:
   - link: https://www.linkedin.com/in/werner-kraus-7a678610a/
     icon: fe-linkedin
resources:
   - name: cover
     src: cover.jpg
   - name: avatar
     src: IMG_3030.JPG
---
Werner Kraus finished his PhD on "force control of cable-driven parallel robots" in 2015 at the Fraunhofer Institute for Manufacturing Engineering and Automation IPA. Meanwhile, he is heading the department of robot and assistive systems at Fraunhofer IPA.