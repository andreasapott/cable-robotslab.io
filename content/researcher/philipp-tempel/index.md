---
title: "Philipp Tempel"
date: 2020-04-27T16:46:50+02:00
toc: true
organization:
    - tudelft-pme
location: Delft, the Netherlands
website: https://philipptempel.me
social:
   - link: https://gitlab.com/philipptempel
     icon: fe-gitlab
   - link: https://github.com/philipptempel
     icon: fe-github
   - link: https://instagram.com/phlpptmpl
     icon: fe-instagram
resources:
   - name: cover
     src: cover.png
   - name: avatar
     src: avatar.png
---
Philipp Tempel is a postdoctoral research associate in cable-driven parallel robots at Delft University of Technology (TU Delft).
His research interests include flexible robots, multibody simulation, and mechanical integrators.
He leads the cable robot group developing robots for the conservation and reconstruction of art.
