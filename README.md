![Build Status](https://gitlab.com/cable-robots/cable-robots.gitlab.io/badges/master/pipeline.svg) ![License: MIT](https://img.shields.io/badge/license-MIT-green.svg)

---

**Table of Contents**

[[_TOC_]]

---



## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] the extended version of hugo (`hugo-extended`)
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/`.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.
2. Receiving syntax errors on clean build:
    
    It is very likely that you have installed the default version of `hugo` and
    not the extended version. We require the extended version due to CSS/JS
    minification, image processing, and resource contatenation taking place
    during building of the site. Standard `hugo` cannot do these taks and fails
    with cryptic syntax error messages.


[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
